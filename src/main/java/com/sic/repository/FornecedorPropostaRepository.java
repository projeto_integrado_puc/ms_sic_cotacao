package com.sic.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sic.model.FornecedorProposta;

public interface FornecedorPropostaRepository extends JpaRepository<FornecedorProposta, Long> {
	
	
	@Query(value = "SELECT distinct(fp), ci.descricaoProduto FROM FornecedorProposta fp "
			+ "INNER JOIN CotacaoItem ci ON ci.id = fp.cotacaoItem.id "
			+ "where fp.cnpj = :cnpj and ci.cotacao.id = :idCotacao order by ci.descricaoProduto ASC")
	List<FornecedorProposta> getCotacao(@Param("cnpj") String cnpj, @Param("idCotacao") Long idCotacao);
	
	@Modifying
	@Transactional
	@Query("update FornecedorProposta fp set fp.valor = :valor, fp.frete = :frete, fp.dataEntrega = :dataEntrega where fp.id = :id")
	void updateProposta(@Param("valor") BigDecimal valor, @Param("frete") BigDecimal frete,@Param("dataEntrega") Date dataEntrega, @Param("id") Long id);
}

