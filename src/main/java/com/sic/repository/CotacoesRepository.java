package com.sic.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.sic.model.Cotacao;

public interface CotacoesRepository extends JpaRepository<Cotacao, Long> {
	
	
	@Query(value = "SELECT distinct(c) FROM Cotacao c "
			+ "INNER JOIN CotacaoItem ci ON ci.cotacao.id = c.id "
			+ "INNER JOIN FornecedorProposta fp ON fp.cotacaoItem.id = ci.id "
			+ "where fp.cnpj = :cnpj order by c.dataCotacao DESC")
	Page<Cotacao> carregaCotacaoComCNPJ(@Param("cnpj") String cnpj, Pageable pageable);	
	
	@Query(value = "SELECT c FROM Cotacao c order by c.dataCotacao DESC")
	Page<Cotacao> carregaCotacao(Pageable pageable);
}

