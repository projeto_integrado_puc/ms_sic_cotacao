package com.sic.resource;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.dto.FornecedorPropostaDTO;
import com.sic.services.PropostaService;

@RestController
@CrossOrigin
@RequestMapping("/proposta")
public class PropostaResource {

	@Autowired
	private PropostaService propostaService;

	@RequestMapping(method = RequestMethod.GET, path = "/{cnpj}/{idCotacao}/{usuarioAdmin}")
	public ResponseEntity<FornecedorPropostaDTO[]> loadProposta(@PathVariable("cnpj") String cnpj, @PathVariable("idCotacao") long idCotacao, @PathVariable("usuarioAdmin") boolean usuarioAdmin) {
		FornecedorPropostaDTO[] fornecedorProposta = this.propostaService.getProposta(cnpj, idCotacao, usuarioAdmin);
		if(Objects.nonNull(fornecedorProposta) && fornecedorProposta.length > 0) {
			return ResponseEntity.ok(fornecedorProposta);			
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/grava")
	public ResponseEntity<String> gravaProposta(@RequestBody List<FornecedorPropostaDTO> listProposta) {
		if(!CollectionUtils.isEmpty(listProposta)) {
			this.propostaService.gravar(listProposta);
			return ResponseEntity.ok("Proposta atualizada");			
		}else {
			return ResponseEntity.noContent().build();
		}
	}
}
