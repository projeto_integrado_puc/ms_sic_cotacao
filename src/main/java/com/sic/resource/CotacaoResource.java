package com.sic.resource;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sic.model.dto.CotacaoDTO;
import com.sic.model.dto.CotacaoFinalizarDTO;
import com.sic.model.dto.CotacaoVencedoraDTO;
import com.sic.services.CotacaoService;

@RestController
@CrossOrigin
@RequestMapping("/cotacao")
public class CotacaoResource {

	@Autowired
	private CotacaoService cotacaoService;

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<String> gravar(@RequestBody CotacaoDTO cotacaoDTO) {
		if (Objects.isNull(cotacaoDTO)) {
			return ResponseEntity.noContent().build();
		} else {
			String codigoCotacao = this.cotacaoService.gravarCotacao(cotacaoDTO);
			return ResponseEntity.ok(codigoCotacao);
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/load/{cnpj}/{page}")
	public ResponseEntity<List<CotacaoDTO>> loadCotacao(@PathVariable("cnpj") String cnpj,
			@PathVariable("page") Integer page) {
		if (Objects.isNull(cnpj)) {
			return ResponseEntity.noContent().build();
		} else {
			Pageable pageable = Pageable.ofSize(10).withPage(page);
			List<CotacaoDTO> pageDados = this.cotacaoService.loadCotacao(cnpj, pageable);
			if(Objects.nonNull(page)) {
				return ResponseEntity.ok(pageDados);			
			}else {
				return ResponseEntity.noContent().build();
			}
		}
	}

	@RequestMapping(method = RequestMethod.GET, path = "/load/{page}")
	public ResponseEntity<List<CotacaoDTO>> loadCotacaoAdmin(@PathVariable("page") Integer page) {
		Pageable pageable = Pageable.ofSize(10).withPage(page);
		List<CotacaoDTO> pageDados = this.cotacaoService.loadCotacao(null, pageable);
		if(Objects.nonNull(page)) {
			return ResponseEntity.ok(pageDados);			
		}else {
			return ResponseEntity.noContent().build();
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<CotacaoVencedoraDTO> finalizaCotacao(@RequestBody CotacaoFinalizarDTO cotacaoFinalizarDTO){
		if(Objects.nonNull(cotacaoFinalizarDTO)) {
			CotacaoVencedoraDTO cotacao = this.cotacaoService.finalizaCotacao(cotacaoFinalizarDTO);
			return ResponseEntity.ok(cotacao);
		}else {
			return ResponseEntity.noContent().build();
		}		
	}
}
