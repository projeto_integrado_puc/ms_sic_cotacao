package com.sic.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "TBCOTACAO")
@DynamicInsert
@DynamicUpdate
public class Cotacao implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Temporal(TemporalType.DATE)
	private Date dataCotacao;
	@Temporal(TemporalType.DATE)
	private Date dataFimCotacao;
	@NotEmpty
	private String nomeComprador;
	@OneToMany(cascade = CascadeType.ALL, targetEntity = CotacaoItem.class, mappedBy = "cotacao")
	private List<CotacaoItem> itens;
	private Boolean statusAberto;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getDataCotacao() {
		return dataCotacao;
	}
	public void setDataCotacao(Date dataCotacao) {
		this.dataCotacao = dataCotacao;
	}
	public Date getDataFimCotacao() {
		return dataFimCotacao;
	}
	public void setDataFimCotacao(Date dataFimCotacao) {
		this.dataFimCotacao = dataFimCotacao;
	}
	public String getNomeComprador() {
		return nomeComprador;
	}
	public void setNomeComprador(String nomeComprador) {
		this.nomeComprador = nomeComprador;
	}
	public List<CotacaoItem> getItens() {
		return itens;
	}
	public void setItens(List<CotacaoItem> itens) {
		this.itens = itens;
	}
	public Boolean isStatusAberto() {
		return statusAberto;
	}

	public void setStatusAberto(Boolean statusAberto) {
		this.statusAberto = statusAberto;
	}
}