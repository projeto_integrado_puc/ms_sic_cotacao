package com.sic.model.dto;

import java.io.Serializable;
import java.util.List;

public class ProdutoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long id;
	private boolean selecionado;
	private String descricao;
	private String codigo;
	private boolean emEstoqueMinimo;
	private boolean cotacaoEmAberto;
	private Long quantidade;
	private List<FornecedorDTO> fornecedores;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public boolean isEmEstoqueMinimo() {
		return emEstoqueMinimo;
	}

	public void setEmEstoqueMinimo(boolean emEstoqueMinimo) {
		this.emEstoqueMinimo = emEstoqueMinimo;
	}

	public boolean isCotacaoEmAberto() {
		return cotacaoEmAberto;
	}

	public void setCotacaoEmAberto(boolean cotacaoEmAberto) {
		this.cotacaoEmAberto = cotacaoEmAberto;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public List<FornecedorDTO> getFornecedores() {
		return fornecedores;
	}

	public void setFornecedores(List<FornecedorDTO> fornecedores) {
		this.fornecedores = fornecedores;
	}
}
