package com.sic.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "TBFORNECEDOR_PROPOSTA")
@DynamicInsert
@DynamicUpdate
public class FornecedorProposta implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotNull
	private long idUser;
	@NotEmpty
	private String cnpj;
	@NotEmpty
	private String nomeFornecedor;
	private BigDecimal valor;
	private BigDecimal frete;
	private Date dataEntrega;
	private String reputacao;
	private String email;
	@OneToOne
	private CotacaoItem cotacaoItem;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdUser() {
		return idUser;
	}
	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNomeFornecedor() {
		return nomeFornecedor;
	}
	public void setNomeFornecedor(String nomeFornecedor) {
		this.nomeFornecedor = nomeFornecedor;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public BigDecimal getFrete() {
		return frete;
	}
	public void setFrete(BigDecimal frete) {
		this.frete = frete;
	}
	public Date getDataEntrega() {
		return dataEntrega;
	}
	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}
	public String getReputacao() {
		return reputacao;
	}
	public void setReputacao(String reputacao) {
		this.reputacao = reputacao;
	}
	public CotacaoItem getCotacaoItem() {
		return cotacaoItem;
	}
	public void setCotacaoItem(CotacaoItem cotacaoItem) {
		this.cotacaoItem = cotacaoItem;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getTotalFreteValor() {
		Double total= Objects.nonNull(this.frete) ? this.frete.doubleValue() : 0.00;
		return total + (Objects.nonNull(this.valor) ? this.valor.doubleValue() : 0.00);
 	}
}
