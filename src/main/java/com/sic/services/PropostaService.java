package com.sic.services;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.sic.model.Cotacao;
import com.sic.model.CotacaoItem;
import com.sic.model.FornecedorProposta;
import com.sic.model.dto.FornecedorPropostaDTO;
import com.sic.repository.CotacoesRepository;
import com.sic.repository.FornecedorPropostaRepository;

@Service
public class PropostaService {

	@Autowired
	private FornecedorPropostaRepository fornecedorProposta;

	@Autowired
	private CotacoesRepository cotacoes;

	public FornecedorPropostaDTO[] getProposta(String cnpj, long idCotacao, boolean usuarioAdmin) {
		if (usuarioAdmin) {
			return getPropostaAdministrador(idCotacao);
		} else {
			return getPropostaFornecedor(cnpj, idCotacao);
		}
	}

	public FornecedorPropostaDTO[] getPropostaAdministrador(long idCotacao) {
		FornecedorPropostaDTO[] propostas = null;
		Cotacao cotacao = cotacoes.getById(idCotacao);
		Hibernate.initialize(cotacao);

		if (Objects.nonNull(cotacao)) {
			propostas = new FornecedorPropostaDTO[cotacao.getItens().size()];
			int indice = 0;
			for (CotacaoItem fpAux : cotacao.getItens()) {
				FornecedorPropostaDTO dto = new FornecedorPropostaDTO();
				dto.setId(fpAux.getId());
				dto.setNomeProduto(fpAux.getDescricaoProduto());
				dto.setQuantidade(fpAux.getQuantidade());
				dto.setFrete(fpAux.getFrete());
				dto.setValor(fpAux.getValor());
				dto.setDataEntrega(fpAux.getDataEntrega());
				propostas[indice] = dto;
				indice++;
			}
		}
		return propostas;
	}

	private FornecedorPropostaDTO[] getPropostaFornecedor(String cnpj, long idCotacao) {
		FornecedorPropostaDTO[] propostas = null;
		List<FornecedorProposta> listFornProposta = fornecedorProposta.getCotacao(cnpj, idCotacao);

		if (!CollectionUtils.isEmpty(listFornProposta)) {
			propostas = new FornecedorPropostaDTO[listFornProposta.size()];
			int indice = 0;
			for (FornecedorProposta fpAux : listFornProposta) {
				FornecedorPropostaDTO dto = new FornecedorPropostaDTO();
				dto.setId(fpAux.getId());
				dto.setNomeProduto(fpAux.getCotacaoItem().getDescricaoProduto());
				dto.setQuantidade(fpAux.getCotacaoItem().getQuantidade());
				dto.setFrete(Objects.nonNull(fpAux.getFrete()) ? fpAux.getFrete() : BigDecimal.ZERO);
				dto.setValor(Objects.nonNull(fpAux.getValor()) ? fpAux.getValor() : BigDecimal.ZERO);
				dto.setDataEntrega(Objects.nonNull(fpAux.getDataEntrega()) ? fpAux.getDataEntrega() : null);
				propostas[indice] = dto;
				indice++;
			}
		}
		return propostas;
	}

	public void gravar(List<FornecedorPropostaDTO> listProposta) {
		for (FornecedorPropostaDTO proposta : listProposta) {
			fornecedorProposta.updateProposta(proposta.getValor(), proposta.getFrete(), proposta.getDataEntrega(),
					proposta.getId());
		}
	}
}
