package com.sic.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.sic.model.Cotacao;
import com.sic.model.CotacaoItem;
import com.sic.model.FornecedorProposta;
import com.sic.model.dto.CotacaoDTO;
import com.sic.model.dto.CotacaoFinalizarDTO;
import com.sic.model.dto.CotacaoItemDTO;
import com.sic.model.dto.CotacaoVencedoraDTO;
import com.sic.model.dto.FornecedorDTO;
import com.sic.model.dto.ProdutoDTO;
import com.sic.model.enums.PrioridadeEnum;
import com.sic.model.enums.ReputacaoFornecedorEnum;
import com.sic.repository.CotacoesRepository;

@Service
public class CotacaoService {

	@Autowired
	private CotacoesRepository cotacao;

	public String gravarCotacao(CotacaoDTO cotacaoDTO) {
		if (Objects.nonNull(cotacaoDTO)) {
			Cotacao cotacao = new Cotacao();
			cotacao.setDataCotacao(cotacaoDTO.getDtCotacao());
			cotacao.setDataFimCotacao(cotacaoDTO.getFimDtCotacao());
			cotacao.setNomeComprador(cotacaoDTO.getNomeComprador());
			cotacao.setItens(new ArrayList<CotacaoItem>());
			cotacao.setStatusAberto(true);

			if (!CollectionUtils.isEmpty(cotacaoDTO.getProdutos())) {
				for (ProdutoDTO p : cotacaoDTO.getProdutos()) {
					if (Objects.nonNull(p) && !CollectionUtils.isEmpty(p.getFornecedores())) {
						CotacaoItem ci = new CotacaoItem();
						ci.setCotacao(cotacao);
						ci.setCodigoProduto(p.getCodigo());
						ci.setDescricaoProduto(p.getDescricao());
						ci.setQuantidade(p.getQuantidade());
						ci.setFornecedorProposta(new ArrayList<>());
						for (FornecedorDTO f : p.getFornecedores()) {
							FornecedorProposta fp = new FornecedorProposta();
							fp.setCnpj(f.getCnpj());
							fp.setReputacao(f.getReputacao());
							fp.setCotacaoItem(ci);
							fp.setIdUser(f.getId());
							fp.setNomeFornecedor(f.getNome());
							fp.setEmail(f.getEmail());
							ci.getFornecedorProposta().add(fp);
						}
						cotacao.getItens().add(ci);
					}
				}
			}
			Long idCotacao = this.cotacao.save(cotacao).getId();
			return Objects.nonNull(idCotacao) ? idCotacao.toString() : "";
		}
		return "";
	}

	public List<CotacaoDTO> loadCotacao(String cnpj, Pageable pageable) {
		Page<Cotacao> page = null;
		if (Objects.isNull(cnpj) || cnpj.isBlank()) {
			page = this.cotacao.carregaCotacao(pageable);
		} else {
			page = this.cotacao.carregaCotacaoComCNPJ(cnpj, pageable);
		}

		if (Objects.isNull(page)) {
			return new ArrayList<>();
		} else {
	        TimeZone.setDefault( TimeZone.getTimeZone("BRT"));	        
			List<CotacaoDTO> listCot = new ArrayList<CotacaoDTO>();
			for (Cotacao cotacao : page.getContent()) {
				CotacaoDTO cot = new CotacaoDTO();
				cot.setId(cotacao.getId());
				cot.setDtCotacao(new Date(cotacao.getDataCotacao().getTime()));
				cot.setFimDtCotacao(new Date(cotacao.getDataFimCotacao().getTime()));
				cot.setTotalRegistros(page.getTotalElements());
				cot.setStatusAberto(cotacao.isStatusAberto());
				listCot.add(cot);
			}
			return listCot;
		}
	}
	
	@Transactional
	public CotacaoVencedoraDTO finalizaCotacao(CotacaoFinalizarDTO cotacaoFinalizar){
		CotacaoVencedoraDTO cv = null;
		Cotacao cotacao =  this.cotacao.getById(cotacaoFinalizar.getId());
		if(Objects.nonNull(cotacao)) {
			inserirCotacaoVencedora(cotacao, cotacaoFinalizar);
			cotacao.setStatusAberto(false);
			cotacao = this.cotacao.save(cotacao);
			cv = montaRetorno(cotacao);
		}
		return cv;
	}
	
	private CotacaoVencedoraDTO montaRetorno(Cotacao cotacao) {
		CotacaoVencedoraDTO cotVenc = new CotacaoVencedoraDTO();
		cotVenc.setDtCotacao(cotacao.getDataCotacao());
		cotVenc.setFimDtCotacao(cotacao.getDataFimCotacao());
		cotVenc.setId(cotacao.getId());
		cotVenc.setNomeComprador(cotacao.getNomeComprador());
		cotVenc.setItens(new ArrayList<>());
		for(CotacaoItem ci : cotacao.getItens()) {
			CotacaoItemDTO ciVenc = new CotacaoItemDTO();
			ciVenc.setCnpjFornecedor(ci.getCnpjFornecedor());
			ciVenc.setCodigoProduto(ci.getCodigoProduto());
			ciVenc.setDataEntrega(ci.getDataEntrega());
			ciVenc.setDescricaoProduto(ci.getDescricaoProduto());
			ciVenc.setFrete(ci.getFrete());
			ciVenc.setNomeFornecedorVencedor(ci.getNomeFornecedorVencedor());
			ciVenc.setQuantidade(ci.getQuantidade());
			ciVenc.setValor(ci.getValor());
			ciVenc.setEmail(ci.getEmail());
			cotVenc.getItens().add(ciVenc);
		}		
		return cotVenc;
	}
	
	private void inserirCotacaoVencedora(Cotacao cotacao, CotacaoFinalizarDTO cotacaoFinalizar) {
		if(Objects.nonNull(cotacao) && !CollectionUtils.isEmpty(cotacao.getItens())) {
			for(CotacaoItem ci : cotacao.getItens()) {
				if(!CollectionUtils.isEmpty(ci.getFornecedorProposta())) {
					FornecedorProposta propostaVencedora = null;
					for(FornecedorProposta fp : ci.getFornecedorProposta()) {
						if(Objects.nonNull(propostaVencedora)) {
							// Prioridade 1
							boolean valido = validaProposta(cotacaoFinalizar.getPrioridadeUm(), propostaVencedora, fp);
							// Prioridade 2
							if(!valido) {
								valido = validaProposta(cotacaoFinalizar.getPrioridadeDois(), propostaVencedora, fp);
							}
							// Prioridade 3
							if(!valido) {
								valido = validaProposta(cotacaoFinalizar.getPrioridadeTres(), propostaVencedora, fp);
							}
							if(valido) {
								propostaVencedora = fp;
							}
						}else {
							if(Objects.nonNull(fp.getDataEntrega()) && Objects.nonNull(fp.getValor()) && Objects.nonNull(fp.getFrete())) {
								propostaVencedora = fp;
							}
						}
					}
					
					if(Objects.nonNull(propostaVencedora)) {
						ci.setCnpjFornecedor(propostaVencedora.getCnpj());
						ci.setDataEntrega(propostaVencedora.getDataEntrega());
						ci.setFrete(propostaVencedora.getFrete());
						ci.setNomeFornecedorVencedor(propostaVencedora.getNomeFornecedor());
						ci.setValor(propostaVencedora.getValor());
						ci.setEmail(propostaVencedora.getEmail());
					}
				}
			}
			
		}
		
	}
	
	private boolean validaProposta(PrioridadeEnum prioridade, FornecedorProposta fp, FornecedorProposta fpAux) {
		boolean retorno = false;
		
		switch (prioridade.ordinal()) {
		case 1:// Data Entrega
			if(fp.getDataEntrega().after(fpAux.getDataEntrega())) {
				retorno = true;
			}
			break;
			
		case 2:// Reputação Fornecedor
			if(ReputacaoFornecedorEnum.buscaValor(fp.getReputacao()) < ReputacaoFornecedorEnum.buscaValor(fpAux.getReputacao())){
				retorno = true;
			}
			break;
			
		case 3:// Valor
			if(fp.getTotalFreteValor() < fpAux.getTotalFreteValor()){
				retorno = true;
			}
			break;

		default:
			break;
		}		
		return retorno;
	}
}
